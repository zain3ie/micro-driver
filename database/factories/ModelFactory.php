<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Driver::class, function (Faker\Generator $faker) {
    static $number = 1;

    return [
        'driver_id' => $number++,
        'latitude' => -2.99 + $faker->randomNumber($nbDigits = 4) / 100000,
        'longitude' => 104.76 + $faker->randomNumber($nbDigits = 4) / 100000,
    ];
});
