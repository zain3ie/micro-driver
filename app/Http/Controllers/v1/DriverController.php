<?php

namespace App\Http\Controllers\v1;

use App\Driver;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function search(Request $request) {
        $latOrigin = $request->latitude;
        $longOrigin = $request->longitude;
        $formula =  '6371 * acos(cos(radians(' .$latOrigin .')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' .$longOrigin .')) + sin(radians(' .$latOrigin .')) * sin(radians(latitude))) as distance';
        $range = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') ."-5 minutes"));

        $driver = Driver::where('created_at', '>', $range)->select(array('driver_id', DB::raw($formula)))->orderBy('distance')->first();

        return $driver;
    }

    public function store(Request $request)
    {
        $driver = new Driver;
        $driver->driver_id = $request->driver_id;
        $driver->latitude = $request->latitude;
        $driver->longitude = $request->longitude;


        if ($driver->save()){
            return response()->json(['success'], 200);
        }
        else return response()->json(['failed'], 500);
    }

    public function update(Request $request)
    {
        if ($driver = Driver::updateOrCreate(
            ['driver_id' => $request->driver_id],
            ['latitude' => $request->latitude, 'longitude' => $request->longitude]
        )){
            return response()->json(['success'], 200);
        }
        else return response()->json(['failed'], 500);
    }
}